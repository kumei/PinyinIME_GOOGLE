﻿package com.keanbin.pinyinime.utils;

import android.util.Log;

public class LogUtil {

	private static boolean DEBUG = true;
	private static String GREP_NAME = " hailongqiu ";
	
	public static void log(String tag, String msg) {
		log(tag, GREP_NAME + msg, null);
	}

	public static void log(String tag, String msg, Throwable e) {
		if (DEBUG) {
			if (e != null) {
				Log.e(tag, GREP_NAME + msg, e);
			} else {
				Log.i(tag, GREP_NAME + msg);
			}
		}
	}
}
